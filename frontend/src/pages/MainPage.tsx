import React from 'react'
import { Container, Typography } from '@material-ui/core'
import { TodoList } from '../components/Todos/TodoList'

export const MainPage: React.FC = () => {
    return (
        <Container>
            <Typography variant={'h3'}>Todo Application</Typography>
            <TodoList />
        </Container>
    )
}