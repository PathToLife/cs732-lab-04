import { AppContext } from './store/AppContextProvider'
import React, { useContext } from 'react'
import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core'
import { MainPage } from './pages/MainPage'

const theme = createMuiTheme({
    palette: {
        type: 'light',
    },
})

const App: React.FC = () => {
    return (
        <React.Fragment>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <MainPage />
            </ThemeProvider>
        </React.Fragment>
    )
}

export default App
