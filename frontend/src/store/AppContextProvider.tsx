import React from 'react'
import useGet from '../hooks/useGet'
import axios from 'axios'
import { ITodo } from './models/todos'

interface IAppContext {
    todos: ITodo[]
    refreshTodos: () => void
    isLoadingTodos: boolean
}

const AppContext = React.createContext<IAppContext>({
    todos: [],
    refreshTodos: () => {},
    isLoadingTodos: false,
})

const AppContextProvider: React.FC = ({ children }) => {
    const { data, reFetch, isLoading } = useGet<ITodo[]>(
        'http://localhost:3001/api/todos',
        []
    )

    // The context value that will be supplied to any descendants of this component.
    const context: IAppContext = {
        todos: data,
        refreshTodos: reFetch,
        isLoadingTodos: isLoading,
    }

    // Wraps the given child components in a Provider for the above context.
    return <AppContext.Provider value={context}>{children}</AppContext.Provider>
}

export { AppContext, AppContextProvider }
