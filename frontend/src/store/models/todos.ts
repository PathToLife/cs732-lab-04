export type TTodoStatus = 'na' | 'completed' | 'pending' | 'archived'

export interface ITodo extends Document {
    id: string
    title: string
    description: string
    createdDate: Date
    dueDate: Date
    status: TTodoStatus
}
