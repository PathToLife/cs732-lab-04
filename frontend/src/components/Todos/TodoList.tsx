import React, { useContext } from 'react'
import { AppContext } from '../../store/AppContextProvider'
import { Todo } from './Todo'
import { Paper } from '@material-ui/core'

export const TodoList: React.FC = () => {
    const appContext = useContext(AppContext)

    return (
        <Paper>
            {appContext.todos.map((todo) => {
                return <Todo todo={todo} />
            })}
        </Paper>
    )
}
