import React from 'react'
import { Paper, Typography } from '@material-ui/core'
import { ITodo } from '../../store/models/todos'

interface TodoProps {
    todo: ITodo
}

export const Todo: React.FC<TodoProps> = (props) => {
    const { title, description, dueDate, createdDate, status } = props.todo
    return (
        <Paper>
            <Typography>{title}</Typography>
            <Typography>{description}</Typography>
            <Typography>{status}</Typography>
            <Typography>{createdDate}</Typography>
            <Typography>{dueDate}</Typography>
        </Paper>
    )
}
