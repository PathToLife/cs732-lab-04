module.exports = {
    clearMocks: true,
    moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
    testEnvironment: 'node',
    roots: [
        './tests'
    ],
    preset: 'ts-jest'
}
