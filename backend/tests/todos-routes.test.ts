import request from 'supertest'
import { switchToTestDb, testDBName } from './switch-to-test-db'
import mongoose from 'mongoose'
import { app } from '../src/server'
import { StatusCodes } from 'http-status-codes'
import { ITodo } from '../src/models/todos'

/**
 * Testing an express application using supertest
 * Does all the testing on a test database after clearing it
 */

beforeAll((done) => {
    console.log('switching to test db...')
    switchToTestDb()
        .then(() => done())
        .catch((err: any) => done(err))
})

describe('Test Express routes', () => {
    it(`should be on ${testDBName} db`, () => {
        expect(mongoose.connection.db.databaseName).toEqual(testDBName)
    })

    it('get /api/todos == []', async () => {
        const result = await request(app).get('/api/todos/').send()
        expect(result.status).toBe(200)
        expect(result.body).toStrictEqual([])
    })

    it('get /api/todos/nothing 404', async () => {
        const result = await request(app).get('/api/todos/nothing').send()
        expect(result.status).toBe(StatusCodes.NOT_FOUND)
    })

    it('post /api/todos 201 and retrieve', async () => {
        const todo: Partial<ITodo> = {
            title: 'someTodo',
            dueDate: new Date(),
            description: 'some description',
        }
        const createRes = await request(app).post('/api/todos').send(todo)
        expect(createRes.status).toBe(StatusCodes.CREATED)
        expect(createRes.body.id).toBeDefined()

        const getRes = await request(app)
            .get(`/api/todos/${createRes.body.id}`)
            .send()
        expect(getRes.status).toBe(StatusCodes.OK)
        expect(getRes.body.title).toEqual(todo.title)
        expect(getRes.body.description).toEqual(todo.description)
    })

    it('should disallow bad post', async () => {
        const badTodo: Partial<ITodo> = {
            description: 'only description no title',
        }
        const res = await request(app).post('/api/todos').send(badTodo)
        expect(res.status).toBe(StatusCodes.BAD_REQUEST)
    })

    it('put /api/todos existing', async () => {
        const todo: Partial<ITodo> = {
            title: 'someTodo',
            dueDate: new Date(),
            description: 'some description',
        }
        const res = await request(app).post('/api/todos').send(todo)
        expect(res.status).toBe(StatusCodes.CREATED)
        expect(res.body.id).toBeDefined()
        expect(res.body.title).toBe(todo.title)

        const postID = res.body.id
        todo.title = 'someNewTitle'
        todo.description = 'someNewDescription'
        todo.id = postID
        const resUpdate = await request(app)
            .put('/api/todos/' + postID)
            .send(todo)
        expect(resUpdate.status).toBe(StatusCodes.NO_CONTENT)

        const resGet = await request(app)
            .get('/api/todos/' + postID)
            .send()
        expect(resGet.status).toBe(StatusCodes.OK)
        expect(resGet.body.title).toBe(todo.title)
        expect(resGet.body.description).toBe(todo.description)
        expect(resGet.body.id).toBe(postID)
    }, 5000)

    it('put /api/todos non-existing 404', async () => {
        const badID = 'notANid'
        const todo: Partial<ITodo> = {
            title: 'someTodo',
            dueDate: new Date(),
            description: 'some description',
            id: badID,
        }
        const resUpdate = await request(app)
            .put(`/api/todos/${badID}`)
            .send(todo)
        expect(resUpdate.status).toBe(StatusCodes.NOT_FOUND)
    }, 5000)

    it('delete /api/todos/:id actually works', async () => {
        const todo: Partial<ITodo> = {
            title: 'someTodo',
            dueDate: new Date(),
            description: 'some description',
        }
        const resCreate = await request(app).post('/api/todos').send(todo)
        expect(resCreate.status).toBe(StatusCodes.CREATED)
        expect(resCreate.body.id).toBeDefined()

        const postID = resCreate.body.id

        const resGet1 = await request(app)
            .get('/api/todos/' + postID)
            .send()
        expect(resGet1.status).toBe(StatusCodes.OK)

        const resDel = await request(app)
            .delete('/api/todos/' + postID)
            .send()
        expect(resDel.status).toBe(StatusCodes.NO_CONTENT)

        const resGet2 = await request(app)
            .get('/api/todos/' + postID)
            .send()
        expect(resGet2.status).toBe(StatusCodes.NOT_FOUND)
    })

    it('delete /api/todos/:id non-existent no error', async () => {
        const resDel = await request(app).delete('/api/todos/NOT_AN_ID').send()
        expect(resDel.status).toBe(StatusCodes.NO_CONTENT)
    })
})
