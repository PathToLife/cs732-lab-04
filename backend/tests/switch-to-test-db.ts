import mongoose from 'mongoose'
import { MONGO_URI } from '../src/utils/constants'
import { mongoConnectionOptions } from '../src/db/db-connect'
import { clearDatabase } from '../src/db/init-db'

export const testDBName = 'jest-test'
export const switchToTestDb = async () => {
    await mongoose.disconnect()
    console.log('disconnected existing db')
    await mongoose.connect(MONGO_URI, {
        ...mongoConnectionOptions,
        dbName: testDBName,
    })
    console.log(`connected to ${testDBName} db`)
    await clearDatabase()
    console.log(`cleared ${testDBName} db`)
}
