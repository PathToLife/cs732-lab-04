import { app } from './server'
import { logger } from './utils/logger'
import connectToDatabase, { isMongooseConnected } from './db/db-connect'

const port = process.env.PORT || 3001

// Start the DB running. Then, once it's connected, start the server.
connectToDatabase().then(() => {
    if (!isMongooseConnected()) {
        logger.error('mongodb not connected, stopping server!')
        process.exit(100)
        return
    }

    logger.info('mongodb connected')

    app.listen(port, () => {
        logger.info(`App server listening on port ${port}!`)
    })
})
