import { Router } from 'express'

const router = Router()

import todos from './todos-routes'

router.use('/todos', todos)

export default router
