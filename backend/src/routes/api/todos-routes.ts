import { Router } from 'express'
import * as todoDao from '../../models/todos'
import { StatusCodes } from 'http-status-codes'
import { Error } from 'mongoose'

const todoRouter = Router()
/**
 * Cruddy CRUD
 */

// create
todoRouter.post('/', async (req, res) => {
    try {
        const data = await todoDao.createTodo(req.body)
        res.status(StatusCodes.CREATED).json(data)
    } catch (e: Error.ValidationError | any) {
        res.status(StatusCodes.BAD_REQUEST).send(e.message)
    }
})

// read
todoRouter.get('/', async (req, res) => {
    const data = await todoDao.retrieveAllTodos()
    res.status(StatusCodes.OK).json(data)
})

// read
todoRouter.get('/:id', async (req, res) => {
    if (!req.params.id) return res.status(StatusCodes.BAD_REQUEST).send()

    const data = await todoDao.retrieveTodo(req.params.id)

    if (data.length === 0) return res.status(StatusCodes.NOT_FOUND).send()

    res.status(StatusCodes.OK).json(data[0])
})

// update
todoRouter.put('/:id', async (req, res) => {
    if (req.params.id !== req.body.id || !req.params.id)
        return res.status(StatusCodes.BAD_REQUEST).send()

    const data = await todoDao.updateTodo(req.body)
    if (!data) {
        return res.status(StatusCodes.NOT_FOUND).send()
    }
    res.status(StatusCodes.NO_CONTENT).send()
})

// delete
todoRouter.delete('/:id', async (req, res) => {
    await todoDao.deleteTodo(req.params.id)
    res.status(StatusCodes.NO_CONTENT).send()
})

export default todoRouter
