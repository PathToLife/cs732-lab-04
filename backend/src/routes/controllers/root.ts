import { RequestHandler } from 'express'
import { StatusCodes } from 'http-status-codes'
import {
    isMongooseConnected,
    waitForMongooseConnect,
} from '../../db/db-connect'

let isServerLoading = true

waitForMongooseConnect().then(() => {
    isServerLoading = false
})

export const rootHandler: RequestHandler = async (_, res) => {
    if (isServerLoading)
        return res
            .status(StatusCodes.SERVICE_UNAVAILABLE)
            .send('server loading...')

    if (!isMongooseConnected())
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            err: 'mongoose not connected',
        })

    return res.json({
        msg: 'hello :)',
    })
}
