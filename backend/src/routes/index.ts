import { Router } from 'express'
import api from './api'
import { rootHandler } from './controllers/root'

const router = Router()

router.use('/api', api)

router.get('/', rootHandler)

export default router
