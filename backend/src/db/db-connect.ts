import mongoose, { ConnectionOptions } from 'mongoose'
import { MONGO_URI } from '../utils/constants'

const DEFAULT_CONNECTION_STRING = MONGO_URI

export const mongoConnectionOptions: ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
}

/**
 * This function begins the process of connecting to the database, and returns a promise that will
 * resolve when the connection is established.
 */
export default function connectToDatabase(
    connectionString = DEFAULT_CONNECTION_STRING
) {
    return mongoose.connect(connectionString, mongoConnectionOptions)
}

export const isMongooseConnected = (): boolean => {
    return mongoose.connection.readyState === 1
}

/**
 * Useful for server status on a express frontend
 * @return true on connection success
 * @param timeoutMS - max wait time before returning false
 */
export const waitForMongooseConnect = async (
    timeoutMS = 8000
): Promise<boolean> => {
    if (isMongooseConnected()) return true

    let checkStart = Date.now()

    while (Date.now() - checkStart < timeoutMS) {
        if (isMongooseConnected()) return true
        await new Promise((res) => setTimeout(res, 500))
    }

    return false
}
