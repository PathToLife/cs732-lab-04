/**
 * This program should be run in order to populate the database with dummy data for testing purposes.
 */

import mongoose from 'mongoose'
import { isMongooseConnected, waitForMongooseConnect } from './db-connect'

/**
 * LOL this one of 'those' functions you really should be careful calling
 */
export async function clearDatabase() {
    if (!isMongooseConnected()) {
        const isConnected = await waitForMongooseConnect()
        if (!isConnected) throw Error('unable to clear database, not connected')
    }

    await mongoose.connection.db.dropDatabase()
}

async function addData() {}
