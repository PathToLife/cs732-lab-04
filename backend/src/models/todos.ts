import * as mongoose from 'mongoose'
import { Document, Schema } from 'mongoose'
import shortid from 'shortid'

export type TTodoStatus = 'na' | 'completed' | 'pending' | 'archived'

export interface ITodo {
    id: string
    title: string
    description: string
    createdDate: Date
    dueDate: Date
    status: TTodoStatus
}

export interface ITodoDocument extends Document<ITodo> {}

/**
 * An id (compulsory, unique)
 * A title (compulsory)
 * A description (optional)
 * A created date (compulsory)
 * A due date / time (compulsory)
 * A completed status (compulsory)
 */
const TodoSchema = new Schema<ITodoDocument>({
    id: {
        type: Schema.Types.String,
        unique: true,
        default: () => shortid(),
    },
    title: {
        type: Schema.Types.String,
        required: true,
    },
    description: {
        type: Schema.Types.String,
        default: '',
    },
    createdDate: {
        type: Schema.Types.Date,
        default: () => new Date(),
        required: true,
    },
    dueDate: {
        type: Schema.Types.Date,
        required: true,
    },
    status: {
        type: Schema.Types.String,
        default: 'na',
        required: true,
    },
})

const TodoModel = mongoose.model<ITodoDocument>('todos', TodoSchema)

/**
 * This file contains functions which interact with MongoDB, via mongoose, to perform
 * CRUD operations.
 */
export const createTodo = async (todo: ITodoDocument) => TodoModel.create(todo)

export const retrieveAllTodos = async (): Promise<ITodoDocument[]> =>
    TodoModel.find({})

export const retrieveTodo = async (id: string) => TodoModel.find({ id })

export const updateTodo = async (todo: ITodoDocument) =>
    TodoModel.findOneAndUpdate({ id: todo.id }, todo)

export const deleteTodo = async (id: string) => TodoModel.deleteOne({ id })
