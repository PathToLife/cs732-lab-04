import express from 'express'
import path from 'path'
import cors from 'cors'
import routes from './routes'
import { logger, morganLogger } from './utils/logger'

// Setup Express
export const app = express()

// logger
app.use(morganLogger)

// Setup body-parser
app.use(express.json())

// Setup cors
app.use(
    cors({
        origin: '*',
    })
)

// Setup our routes.
app.use('/', routes)

// Make the "public" folder available statically
app.use(express.static(path.join(__dirname, '../public')))

// Serve up the frontend "build" directory, if we're running in production mode.
if (process.env.NODE_ENV === 'production') {
    logger.info('production mode')

    // Make all files in that folder public
    app.use(express.static(path.join(__dirname, '../../frontend/build')))

    // If we get any GET request we can't process using one of the server routes, serve up index.html by default.
    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, '../../frontend/build/index.html'))
    })
}
