import winston from 'winston'
import morgan, { StreamOptions } from 'morgan'

const format = winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.colorize({ all: true }),
    winston.format.printf(
        (info) => `${info.timestamp} ${info.level}: ${info.message}`
    )
)

const transports = [
    new winston.transports.Console(),
    new winston.transports.File({
        filename: 'logs/error.log',
        level: 'error',
    }),
    new winston.transports.File({ filename: 'logs/all.log' }),
]

export const logger = winston.createLogger({
    format,
    transports,
    level: 'debug',
})

const stream: StreamOptions = {
    write: (msg: string) => logger.http(msg.trim()),
}

export const morganLogger = morgan(
    ':method :url :status :res[content-length] - :response-time ms',
    {
        stream,
        skip: () => false,
    }
)
